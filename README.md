#Training Git

1. Membuat repository baru

git init nama-folder

2. Melihat status working folder

git status

3. Menambahkan perubahan ke staging area

git add .

4. Menyimpan perubahan ke repository

git commit -m "pesan/ keterangan commit"

5. Melihat histori perubahan

git log 
git log --oneline

6. Melihat perbedaan (difference) antar versi

git diff commitId1..commitId2

7. Membatalkan perubahan di staging

git reset

8. Membatalkan perubahan di staging dan working(!!bahaya!!)
 git reset hard

